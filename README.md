# WebGL

## Pre-requisites

* Install [Node.js](https://nodejs.org/en/)

## Installation

Start by running:

```sh
npm i
```

This will install dependencies.

## Run

In order to work on the code, please run:

```sh
npm run dev
```

You can then reach [http://localhost:8080](http://localhost:8080) to try it!

This will spawn two processes:
* The build: takes all TypeScript files and transform them to JS.
  When files are transformed, concatenate them in a single output
* The webserver: serve the files (especially assets) to your browser tab locally
