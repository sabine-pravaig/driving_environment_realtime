import * as THREE from 'three';
import { RoadShader } from './shader/road_shader';
import { TextureLoader } from 'three';

// Create plane geometry
const planeGeometry = new THREE.PlaneGeometry(135, 60);

// Init uniforms
const roadUniforms = THREE.UniformsUtils.clone(RoadShader.uniforms);
roadUniforms['u_tex'].value = new TextureLoader().load('assets/textures/road.jpg');
roadUniforms['u_time'].value = 0;
roadUniforms['u_speed'].value = 1;

// Create material
const planeMaterial = new THREE.ShaderMaterial({
    uniforms: roadUniforms,
    vertexShader: RoadShader.vertexShader,
    fragmentShader: RoadShader.fragmentShader
});

// Create plane
const plane = new THREE.Mesh(planeGeometry, planeMaterial);
plane.rotation.x = -0.5 * Math.PI;
plane.receiveShadow = true;

export { plane, roadUniforms };