import * as THREE from 'three';

import { roadUniforms } from './plane';
import { initGLTFModels, loadModels, updateModels } from './models';
import { scene } from './init';
import { initRenderer, render } from './render';

// Add mouse control event
const mousePosition = new THREE.Vector2();
window.addEventListener('mousemove', (x: MouseEvent) => {
    mousePosition.x = (x.clientX / window.innerWidth) * 2 - 1;
    mousePosition.y = - (x.clientY / window.innerHeight) * 2 + 1;
});

// Init models
var models = initGLTFModels();

// Init scene animation data
var sceneAnimationData = { sceneChildrenIdx: 0, staticCarAdded: false };
sceneAnimationData.sceneChildrenIdx = scene.children.length;

// Init renderer
var renderer = initRenderer();

// Animation
function animate(time: number) {
    if (models.get("car") == undefined)
        return;

    if (sceneAnimationData.sceneChildrenIdx == scene.children.length)
        loadModels(scene, sceneAnimationData, models);
    else if (sceneAnimationData.sceneChildrenIdx < scene.children.length)
        updateModels(scene, sceneAnimationData.sceneChildrenIdx, time);

    roadUniforms['u_time'].value = time / 1000;
    render(renderer);
}

renderer.setAnimationLoop(animate);