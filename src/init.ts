import * as THREE from 'three';
import { plane } from './plane';

const scene = new THREE.Scene();

new THREE.TextureLoader().load('assets/textures/background.jpg',
    (texture : THREE.Texture) => {
        texture.minFilter = THREE.LinearFilter;
        scene.background = texture;
    });

// Init orthographic camera
const cameraOrtho = new THREE.OrthographicCamera(-45, 45, 40, -20, 0.1, 100);
cameraOrtho.position.set(0, 50, 10);
cameraOrtho.rotateX(-Math.PI / 2);

// Init ambient light
const ambientLight = new THREE.AmbientLight(0x333333);
scene.add(ambientLight);

// Init directional light
const directionalLight = new THREE.DirectionalLight(0xFFFFFF, 0.8);
directionalLight.position.set(30, 40, 35);
directionalLight.castShadow = true;
scene.add(directionalLight);

// Init directional light helper
const directionalLightHelper = new THREE.DirectionalLightHelper(directionalLight, 5);
scene.add(directionalLightHelper);

// Init camera
const camera = new THREE.PerspectiveCamera(75, window.innerWidth / window.innerHeight, 0.1, 1000);
camera.position.set(0, 10, 30);
scene.add(camera);

// Add plane
scene.add(plane);

export { scene, cameraOrtho, camera };