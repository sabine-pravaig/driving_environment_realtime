import { GUI } from 'dat.gui';
import { roadUniforms } from './plane';

const gui = new GUI();
const options = {
    'Car speed': 1
};

roadUniforms['u_speed'].value = options['Car speed'] / 15;

gui.add(options, 'Car speed', 1, 50.0).onChange((x: number) => {
    roadUniforms['u_speed'].value = x / 20;
})

export { options };