import { Uniform } from "three";

const RoadShader = {

    uniforms: {
        u_tex: Uniform,
        u_time: Uniform,
        u_speed: Uniform
    },

    vertexShader : `
    #include <common>

    varying vec2 v_uv;
    
    void main(){
        gl_Position = projectionMatrix * modelViewMatrix * vec4(position, 1.0);
        v_uv = uv;
    }
    `,

    fragmentShader : `
    #include <common>
    
    uniform sampler2D u_tex;
    uniform float u_time;
    uniform float u_speed;
    
    varying vec2 v_uv;

    vec2 update_uv() {
        float v = fract(v_uv.y + u_time * u_speed);
        return vec2(v_uv.x, v);
    }
    
    void main() {
        vec2 uv = update_uv();
        vec4 pixel = texture2D(u_tex, uv);
        gl_FragColor = vec4(pixel.rgb, 1.0);
    }
    `
}

export { RoadShader };