import * as THREE from 'three';
import { GLTF, GLTFLoader } from 'three/examples/jsm/loaders/GLTFLoader';
import dataJson from './data.json';
import { options } from './gui';

export function initGLTFModels() {
    var models = new Map<string, GLTF>();
    const objectsModels = new Map([
        ["tree", "models/tree.glb"],
        ["car", "models/car.glb"],
        ["building", "models/building.glb"],
        ["human", "models/man.glb"],
        ["traffic light", "models/traffic_light.glb"]
    ]);

    const keys = [...objectsModels.keys()];
    keys.forEach((value) => {
        const path = objectsModels.get(value)!;
        const gltfLoader = new GLTFLoader();
        gltfLoader.load(path, (gltf) => {
            gltf.scene.name = value;

            if (value == 'human')
                gltf.scene.rotateY(Math.PI / 10);

            models.set(value, gltf);
        });
    });
    return models;
}

function loadGLTFModel(scene: THREE.Scene, model: GLTF | undefined, position: THREE.Vector3, angle: number = 0.0) {
    if (model == undefined)
        return;

    const copy = new THREE.Group();
    copy.copy(model.scene);
    copy.position.set(position.x, position.y, position.z);
    copy.rotateY(angle);

    scene.add(copy);
}

function readJson(scene: THREE.Scene, models: Map<string, GLTF>) {
    dataJson.forEach(element => {
        const position = new THREE.Vector3(element.position[0], element.position[1], element.position[2]);
        loadGLTFModel(scene, models.get(element.classname), position, element.angle);
    });
}

function loadStaticCarModel(scene: THREE.Scene, carModel: GLTF | undefined, sceneAnimationData: { sceneChildrenIdx: number, staticCarAdded: boolean }) {
    if (sceneAnimationData.staticCarAdded)
        return;

    const position = new THREE.Vector3(8, 0, 25);
    loadGLTFModel(scene, carModel, position);
    scene.children[scene.children.length - 1].rotateY(Math.PI);

    sceneAnimationData.sceneChildrenIdx += 1;
    sceneAnimationData.staticCarAdded = true;
}

export function loadModels(scene: THREE.Scene, sceneAnimationData: { sceneChildrenIdx: number, staticCarAdded: boolean }, models: Map<string, GLTF>) {
    loadStaticCarModel(scene, models.get("car"), sceneAnimationData);
    readJson(scene, models);
}

export function updateModels(scene: THREE.Scene, sceneChildrenIdx: number, time: number) {
    for (let index = sceneChildrenIdx; index < scene.children.length; index++) {
        const elt = scene.children[index];
        elt.position.z += 0.5 * options["Car speed"] / 10;
        if (elt.position.z >= 20)
            scene.remove(elt);

        if (elt.name == 'human') {
            elt.position.z += Math.random() / 1.5 * options["Car speed"] / 10;
            if (Math.floor(time) % 8 == 0)
                elt.rotation.y *= -1;
        }

    }
}