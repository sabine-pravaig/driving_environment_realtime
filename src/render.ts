import * as THREE from 'three';
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls';
import { camera, cameraOrtho, scene } from './init';

const insetWidth = window.innerHeight / 4;
const insetHeight = window.innerHeight / 4;

export function initRenderer() {
    const renderer = new THREE.WebGLRenderer();
    renderer.shadowMap.enabled = true;

    renderer.setSize(window.innerWidth, window.innerHeight);

    document.body.appendChild(renderer.domElement);

    // Camera controls
    const orbitControls = new OrbitControls(camera, renderer.domElement);
    orbitControls.update();
    return renderer;
}

export function render(renderer: THREE.WebGLRenderer) {
    renderer.setClearColor(0x000000);
    renderer.setScissorTest(false);
    renderer.setViewport(0, 0, window.innerWidth, window.innerHeight);

    renderer.render(scene, camera);

    renderer.clearDepth();

    renderer.setClearColor(0x333333);
    renderer.setScissorTest(true);
    renderer.setScissor(16, window.innerHeight - insetHeight - 16, insetWidth, insetHeight);
    renderer.setViewport(16, window.innerHeight - insetHeight - 16, insetWidth, insetHeight);

    renderer.render(scene, cameraOrtho);
};