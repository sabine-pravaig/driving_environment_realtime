import json
import time

data = []
obj = [("car", -1, 0, 0), ("tree", 4, 0, 0), ("building", -16, 0, 0), ("tree", 6, 0, 0)]

for classname, x, y, angle in obj:
    dictionary = {
        "classname": classname,
        "position": [x, y, 0],
        "angle": angle
    }
    data.append(dictionary)

timer = 20
pos = 0.1
while timer:
    time.sleep(1)
    with open('src/data.json', 'w') as f:
        json.dump(data, f)
    for elt in data:
        elt["position"][2] = pos
        print(elt["position"])

    pos += 0.5
    timer -= 1

